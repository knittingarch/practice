from django.core.validators import RegexValidator
from django.template.defaultfilters import slugify
from django.template import Template, Context
from utils import get_domain
from django.contrib.auth.models import User
from django.db import models

class Timestamp(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    class Meta:
        abstract = True

# I might need to make a User model and link it to Profile

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Alphanumeric characters only.')

class Profile(Timestamp):
    first_name = models.CharField(max_length=100, blank=True, null=True, validators=[alphanumeric])
    middle_name = models.CharField(max_length=100, blank=True, null=True, validators=[alphanumeric])
    last_name = models.CharField(max_length=100, blank=True, null=True, validators=[alphanumeric])
    preferred_name = models.CharField(max_length=100, blank=True, null=True, validators=[alphanumeric])
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    school_name = models.CharField(max_length=100)
    short_bio = models.TextField()
    interest1 = models.CharField(max_length=100)
    interest2 = models.CharField(max_length=100)
    interest3 = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    domain = models.CharField(max_length=100)
    webcode = models.CharField(max_length=100)
    user = models.OneToOneField(User, blank=True, null=True)
            
    def middle_initial(self): # doesn't seem to work for domain logic
        t = Template('{{ first_name|slice:"1" }}')
        c = Context ({'first_name': first_name })
        return t.render(c)
            
    @property # what does this do?
    def name(self):
        return "%s %s" % (self.first_name, self.last_name)
    
    
    def save(self, *args, **kwargs):
        if not self.webcode: # What are these ifs saying?
            self.webcode = User.objects.make_random_password(length=10)
        if not self.slug:
            self.slug = slugify(self.name)
        if not self.domain:
            self.domain = get_domain(first_name=self.first_name, middle_name=self.middle_name, last_name=self.last_name, preferred_name=self.preferred_name)
        # for field_name in ['interest1', 'interest2', 'interest3',]:
        #     val = getattr(self, field_name, False)
        #     if val:
        #         setattr(self, field_name, val.title())
        return super(Profile, self).save(*args, **kwargs)
            
    def __str__(self):
        return self.name

            
def get_image_path(instance, filename):
    return '/'.join(['profile_images', instance.profile.slug, filename])


class User(models.Model):
    profile = models.OneToOneField(Profile, blank=True, null=True, related_name="creators")


class Upload(Timestamp):
    profile = models.ForeignKey(Profile, related_name="uploads")
    image = models.ImageField(upload_to=get_image_path)








                  

