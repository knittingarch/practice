# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import student_profile.models


class Migration(migrations.Migration):

    dependencies = [
        ('student_profile', '0005_auto_20160114_1501'),
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('image', models.ImageField(upload_to=student_profile.models.get_image_path)),
                ('profile', models.ForeignKey(related_name='uploads', to='student_profile.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
