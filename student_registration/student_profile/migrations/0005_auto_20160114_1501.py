# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('student_profile', '0004_auto_20160114_1201'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 14, 20, 1, 26, 671046, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 14, 20, 1, 45, 414136, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
