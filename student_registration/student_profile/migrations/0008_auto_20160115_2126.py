# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student_profile', '0007_auto_20160115_2037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='profile',
            field=models.OneToOneField(related_name='creators', null=True, blank=True, to='student_profile.Profile'),
        ),
    ]
