# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=254)),
                ('school_name', models.CharField(max_length=100)),
                ('short_bio', models.TextField()),
                ('interest1', models.CharField(max_length=100)),
                ('interest2', models.CharField(max_length=100)),
                ('interest3', models.CharField(max_length=100)),
                ('domain', models.CharField(max_length=100)),
                ('slug', models.SlugField(unique=True)),
                ('webcode', models.CharField(max_length=100)),
            ],
        ),
    ]
