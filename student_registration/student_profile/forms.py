from django.forms import ModelForm
from student_profile.models import Profile, Upload

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('first_name', 'preferred_name', 'middle_name', 'last_name', 'email', 'school_name', 'short_bio', 'interest1', 'interest2', 'interest3',)
        
class UploadForm(ModelForm):
    class Meta:
        model = Upload
        fields = ('image',)