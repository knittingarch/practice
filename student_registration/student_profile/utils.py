import subprocess
import random

def checkdomain(domain):
    try:
        ret = subprocess.check_output(["whois", domain])
        return "No match for" not in ret
    except subprocess.CalledProcessError:
        return False

def checkavailability(domain):
    return not checkdomain(domain)

DOMAINS = [
    # '{name}.com',
    '{first_name}{last_name}.com', # FirstLast
    # '{first_name}{middle_initial}{last_name}.com', # FirstMILast
    '{first_name}{middle_name}{last_name}.com', # FirstMiddleLast
    '{preferred_name}{last_name}.com', # PreferrednameLast
    '{first_name}-{last_name}.com', # First-Last
    # '{first_name}-{middle_initial}-{last_name}.com', # First-MI-Last
    '{first_name}-{middle_name}-{last_name}.com', # First-Middle-Last
    '{preferred_name}-{last_name}.com', # Preferredname-Last
    '{first_name}_{last_name}.com', # First_Last
    # '{first_name}_{middle_initial}_{last_name}.com', # First_MI_Last
    '{first_name}_{middle_name}_{last_name}.com', # First_Middle_Last
    '{preferred_name}_{last_name}.com', # Preferredname_Last
    # FirstLast[1random]
    # FirstLast[2random]
    # FirstLast[3random]
    '{first_name}{last_name}-{random}.com', # FirstLast[4random]
]

def get_domain(**kwargs):
    kwargs['random'] = ''.join(random.sample("0123456789", 4))
    for template in DOMAINS:
        domain = template.format(**kwargs)
        if checkavailability(domain):
            return domain
    raise ValueError("No valid domain found")