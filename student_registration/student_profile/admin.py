from django.contrib import admin

 # import your models
from student_profile.models import Profile, Upload

 # set up automated slug creation
class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ('first_name', 'preferred_name', 'middle_name', 'last_name', 'email', 'school_name', 'short_bio', 'interest1', 'interest2', 'interest3', 'domain', 'webcode', 'slug')
    prepopulated_fields = {'slug': ('first_name', 'last_name',)}

class UploadAdmin(admin.ModelAdmin):
    list_display = ('profile',)
    list_display_links = ('profile',)

 # register your models
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Upload, UploadAdmin)