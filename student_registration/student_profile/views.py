from django.conf import settings
from django.template.defaultfilters import slugify
from django.shortcuts import render, redirect
from student_profile.forms import ProfileForm, UploadForm
from student_profile.models import Profile, Upload
# from django.contrib.auth.decorators import login_required
# from django.http import Http404


# Create your views here.
def index(request):
    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        if request.user.is_superuser:    
            profiles = Profile.objects.all().order_by('last_name')
            return render(request, 'index.html', {
                'profiles': profiles,
                })
        else:
            profile = request.user
            return render(request, 'profiles/profile_detail.html', {
                'profile': profile,
            })
    
def profile_detail(request, slug):
    profile = Profile.objects.get(slug=slug)
    uploads = profile.uploads.all()
    
    return render(request, 'profiles/profile_detail.html', {
        'profile': profile,
        'uploads': uploads,
    })



# @login_required # Need to add admin to editing permissions
def edit_profile(request, slug):
    profile = Profile.objects.get(slug=slug)
    # if profile.user != request.user:
    #     raise Http404
    form_class = ProfileForm
    if request.method == 'POST':
        form = form_class(data=request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return redirect('profile_detail', slug=profile.slug)
    else:
        form = form_class(instance=profile)    
    return render(request, 'profiles/edit_profile.html', {
        'profile': profile,
        'form': form,
        })
    
def create_profile(request):
    form_class = ProfileForm
    if request.method == 'POST':
        form = form_class(request.POST)
        if form.is_valid():
             # create an instance without saving it yet
            profile = form.save(commit=False)
            profile.user = request.user
            profile.slug = slugify(profile.name)
             # now save it
            profile.save()
            return redirect('profile_detail', slug=profile.slug)
    else:
        form = form_class()    
    return render(request, 'profiles/create_profile.html', {
        'form': form, 
        })
        
# @login_required
def edit_profile_uploads(request, slug):
        # grab the object
        profile = Profile.objects.get(slug=slug)
        # double check it's the right user
        # if profile.user != request.user:
        #     raise Http404
        # set the form we're using    
        form_class = UploadForm
        
        # if we're coming to this view from submitted form,
        if request.method == 'POST':
            # grab data from the submitted form
            form = forn_class(data=request.POST, files=request.FILES, instance=profile)
            if form.is_valid():
                #create a new object from the submitted form
                Upload.objects.create(
                    image=form.cleand_data['image'],
                    profile=profile,
                )
                
                return redirect('edit_profile_uploads', slug=profile.slug)

       # otherwise just create the form
        else:
            form = form_class(instance=profile)
        
        # grab all the object's images
        uploads = profile.uploads.all()
        
        # and render the template
        return render(request, 'profiles/edit_profile_uploads.html', {
            'profile': profile,
            'form': form,
            'uploads': uploads,
        })
        
# Need to create a website template view here and use code like this
# open('rendered_foo.html', 'w').write(get_template('foo.html').render({'form': form})) would do.